﻿using System;
using System.Globalization;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PalindromeExtension;

namespace PalindromExtension_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_using_the_palindrome_extension_method
    {
        private const string twoLetterPalindrome = "oo";
        private const string threeLetterPalindrome = "mom";
        private const string fourLetterPalindrome = "noon";

        [TestMethod]
        public void And_the_given_string_is_not_a_palindrome_then_the_extension_returns_false()
        {
            //arrange
            
            const string notApalindrome = "This is not a palindrome";

            //act

            var observedResult = notApalindrome.IsPalindrome();

            //assert

            Assert.IsFalse(observedResult);
        }

        [TestMethod]
        public void And_the_given_string_is_a_palindrome_with_2_letters_then_the_extension_returns_true()
        {
            //act

            var observedResult = twoLetterPalindrome.IsPalindrome();

            //assert

            Assert.IsTrue(observedResult);
        }

        [TestMethod]
        public void And_the_given_string_is_a_palindrome_with_2_letters_with_one_capital_then_the_extension_returns_true()
        {
            var testString = twoLetterPalindrome.First().ToString(CultureInfo.InvariantCulture).ToUpper() +
                             String.Join("", twoLetterPalindrome.Skip(1));
            //act

            var observedResult = testString.IsPalindrome();

            //assert

            Assert.IsTrue(observedResult);
        }

        [TestMethod]
        public void And_the_given_string_is_a_palindrome_with_3_letters_then_the_extension_returns_true()
        {
            //act

            var observedResult = threeLetterPalindrome.IsPalindrome();

            //assert

            Assert.IsTrue(observedResult);
        }

        [TestMethod]
        public void And_the_given_string_is_a_palindrome_with_4_letters_then_the_extension_returns_true()
        {
            //act

            var observedResult = fourLetterPalindrome.IsPalindrome();

            //assert

            Assert.IsTrue(observedResult);
        }
    }
    // ReSharper restore InconsistentNaming
}
