﻿using System;
using System.Globalization;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PalindromeExtension;

namespace PalindromExtension_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_using_ridiculously_easy_linq_palinindrome_extension
    {
        private const string twoLetterPalindrome = "oo";
        private const string threeLetterPalindrome = "mom";
        private const string fourLetterPalindrome = "noon";
        private const string fiveLetterPalindrome = "civic";

        [TestMethod]
        public void And_the_given_string_is_not_a_palindrome_then_the_ridiculously_easy_linq_only_extension_returns_false()
        {
            //arrange

            const string notApalindrome = "This is not a palindrome";

            //act

            var observedResult = notApalindrome.RidiculouslyEasyLinqOnlyIsPalindrome();

            //assert

            Assert.IsFalse(observedResult);
        }

        [TestMethod]
        public void And_the_given_string_is_a_palindrome_with_2_letters_then_the_ridiculously_easy_linq_only_extension_returns_true()
        {
            //act

            var observedResult = twoLetterPalindrome.RidiculouslyEasyLinqOnlyIsPalindrome();

            //assert

            Assert.IsTrue(observedResult);
        }

        [TestMethod]
        public void And_the_given_string_is_a_palindrome_with_2_letters_with_one_capital_then_the_ridiculously_easy_linq_only_extension_returns_true()
        {
            var testString = twoLetterPalindrome.First().ToString(CultureInfo.InvariantCulture).ToUpper() +
                             String.Join("", twoLetterPalindrome.Skip(1));
            //act

            var observedResult = testString.RidiculouslyEasyLinqOnlyIsPalindrome();

            //assert

            Assert.IsTrue(observedResult);
        }

        [TestMethod]
        public void And_the_given_string_is_a_palindrome_with_3_letters_then_the_ridiculously_easy_linq_only_extension_returns_true()
        {
            //act

            var observedResult = threeLetterPalindrome.RidiculouslyEasyLinqOnlyIsPalindrome();

            //assert

            Assert.IsTrue(observedResult);
        }

        [TestMethod]
        public void And_the_given_string_is_a_palindrome_with_4_letters_then_the_ridiculously_easy_linq_only_extension_returns_true()
        {
            //act

            var observedResult = fourLetterPalindrome.RidiculouslyEasyLinqOnlyIsPalindrome();

            //assert

            Assert.IsTrue(observedResult);
        }

        [TestMethod]
        public void And_ther_given_string_is_a_palindrome_with_5_letters_the_the_ridiculously_easy_linq_only_extension_returns_true()
        {
            //act

            var observedResult = fiveLetterPalindrome.RidiculouslyEasyLinqOnlyIsPalindrome();

            //assert

            Assert.IsTrue(observedResult);
        }
    }
    // ReSharper restore InconsistentNaming
}
