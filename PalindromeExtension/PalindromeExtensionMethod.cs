﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PalindromeExtension
{
    public static class PalindromeExtensionMethod
    {
        #region Palindrome extension methods

        public static bool IsPalindrome(this string value)
        {
            var startingStack = StringToStack(value.ToLower());
            var stackToCompareTo = new Stack<char>();

            while (startingStack.Count > 1)
            {
                stackToCompareTo.Push(startingStack.Pop());

                var middleCharacter = startingStack.Pop();
                
                var firstHalf = new String(startingStack.ToArray());
                var secondHalf = new String(stackToCompareTo.ToArray());
                
                if ((firstHalf == secondHalf) || (middleCharacter + firstHalf == secondHalf)) return true;

                startingStack.Push(middleCharacter);
            }

            return false;
        }

        public static bool LinqOnlyIsPalindrome(this string value)
        {
            return value.IsEvenLength() ? value.ToLower().Substring(0, (value.Count()/2)) == new string(value.ToLower().Substring((value.Count()/2)).Reverse().ToArray())
                : value.ToLower().Substring(0, (value.Count()/2)) == new string(value.ToLower().Substring((value.Count()/2) + 1).Reverse().ToArray());
        }

        public static bool RidiculouslyEasyLinqOnlyIsPalindrome(this string value)
        {
           return value.ToLower().Equals(new string(value.ToLower().Reverse().ToArray()));
        }

        #endregion

        #region private helper methods

        private static Stack<char> StringToStack(string theStringToConvert)
        {
            var backwardStringAsArray = theStringToConvert.Reverse().ToArray();
            var stackToReturn = new Stack<char>();

            foreach (var charater in backwardStringAsArray)
            {
                stackToReturn.Push(charater);   
            }

            return stackToReturn;
        }

        private static bool IsEvenLength(this string value)
        {
            return ((value.Count() & 1)==0);
        }

        #endregion
    }
}
